object Form1: TForm1
  Left = 195
  Top = 165
  Anchors = [akRight, akBottom]
  Caption = 'WAVEVolumeCorrector'
  ClientHeight = 356
  ClientWidth = 583
  Color = clCream
  Constraints.MinHeight = 255
  Constraints.MinWidth = 550
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesigned
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  DesignSize = (
    583
    356)
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 425
    Top = 104
    Width = 11
    Height = 15
    Anchors = [akTop, akRight]
    Caption = '%'
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentFont = False
    ExplicitLeft = 437
  end
  object Button1: TButton
    Left = 338
    Top = 318
    Width = 186
    Height = 30
    Align = alCustom
    Anchors = [akRight, akBottom]
    Caption = 'Start'
    Enabled = False
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Times New Roman'
    Font.Style = []
    ImageAlignment = iaBottom
    ParentFont = False
    TabOrder = 0
    OnClick = Button1Click
  end
  object ProgressBar1: TProgressBar
    Left = 540
    Top = 0
    Width = 30
    Height = 356
    Align = alRight
    Max = 10000
    Orientation = pbVertical
    MarqueeInterval = 1
    TabOrder = 1
  end
  object ToggleSwitch1: TToggleSwitch
    Left = 434
    Top = 47
    Width = 98
    Height = 20
    Anchors = [akTop, akRight]
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Times New Roman'
    Font.Style = []
    FrameColor = clBackground
    ParentFont = False
    StateCaptions.CaptionOn = 'Replace'
    StateCaptions.CaptionOff = 'Copy'
    TabOrder = 2
    ThumbColor = clDefault
  end
  object ActivityIndicator1: TActivityIndicator
    Left = 434
    Top = 8
    Anchors = [akTop, akRight]
    IndicatorType = aitSectorRing
  end
  object StringGrid1: TStringGrid
    Left = 0
    Top = 0
    Width = 332
    Height = 356
    Align = alLeft
    Anchors = [akLeft, akTop, akRight, akBottom]
    BevelInner = bvNone
    Color = clWhite
    ColCount = 1
    FixedColor = clWhite
    FixedCols = 0
    RowCount = 1
    FixedRows = 0
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Times New Roman'
    Font.Style = []
    Options = [goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goDrawFocusSelected, goColSizing]
    ParentFont = False
    TabOrder = 4
  end
  object Button2: TButton
    Left = 338
    Top = 42
    Width = 90
    Height = 30
    Align = alCustom
    Anchors = [akTop, akRight]
    Caption = 'Add folder'
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Times New Roman'
    Font.Style = []
    ImageAlignment = iaBottom
    ParentFont = False
    TabOrder = 5
    OnClick = Button2Click
  end
  object Button3: TButton
    Left = 338
    Top = 8
    Width = 90
    Height = 30
    Align = alCustom
    Anchors = [akTop, akRight]
    Caption = 'Add files'
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Times New Roman'
    Font.Style = []
    ImageAlignment = iaBottom
    ParentFont = False
    TabOrder = 6
    OnClick = Button3Click
  end
  object StatusBar1: TStatusBar
    Left = 570
    Top = 0
    Width = 13
    Height = 356
    Align = alRight
    Panels = <>
  end
  object Button4: TButton
    Left = 338
    Top = 146
    Width = 186
    Height = 30
    Align = alCustom
    Anchors = [akTop, akRight]
    Caption = 'Remove'
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Times New Roman'
    Font.Style = []
    ImageAlignment = iaBottom
    ParentFont = False
    TabOrder = 8
    OnClick = Button4Click
  end
  object Edit1: TEdit
    Left = 338
    Top = 119
    Width = 186
    Height = 21
    Alignment = taCenter
    Anchors = [akTop, akRight]
    TabOrder = 9
    Text = '100'
    OnChange = Edit1Change
  end
  object TrackBar1: TTrackBar
    Left = 338
    Top = 72
    Width = 186
    Height = 33
    Anchors = [akTop, akRight]
    Ctl3D = True
    ParentCtl3D = False
    Position = 5
    ShowSelRange = False
    TabOrder = 10
    TickMarks = tmTopLeft
    OnChange = TrackBar1Change
  end
  object OpenDialog1: TOpenDialog
    Options = [ofHideReadOnly, ofAllowMultiSelect, ofExtensionDifferent, ofPathMustExist, ofFileMustExist, ofEnableSizing]
    Left = 8
    Top = 8
  end
end
