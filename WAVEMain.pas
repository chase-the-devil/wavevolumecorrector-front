unit WAVEMain;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes,
  Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls,
  Winapi.ShellAPI, Vcl.ComCtrls, Vcl.WinXCtrls, Vcl.Grids, ShlObj, FileCtrl;

type
  TForm1 = class(TForm)
      OpenDialog1         :TOpenDialog;
      Button1             :TButton;
      ProgressBar1        :TProgressBar;
      ToggleSwitch1       :TToggleSwitch;
      ActivityIndicator1  :TActivityIndicator;
      StringGrid1         :TStringGrid;
      Button2             :TButton;
      Button3             :TButton;
      StatusBar1          :TStatusBar;
      Button4             :TButton;
      Edit1               :TEdit;
      Label1              :TLabel;
      TrackBar1           :TTrackBar;

      procedure Button4Click    (Sender: TObject);
      procedure FormDestroy     (Sender: TObject);
      procedure FormCreate      (Sender: TObject);
      procedure Panel1MouseDown (Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
      procedure Panel1MouseUp   (Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
      procedure DropFiles       (var Msg: TWMDropFiles); message WM_DROPFILES;
      procedure TrackBar1Change (Sender: TObject);
      procedure Edit1Change     (Sender: TObject);
      procedure Button3Click    (Sender: TObject);
      procedure Button2Click    (Sender: TObject);
      procedure Button1Click    (Sender: TObject);
    end;

  TProgressThread = class(TThread)
    protected
      procedure UpdateProgress;
      procedure StopRender;
      procedure Execute; override;
    end;

type
  ProgressCallback  = procedure (inParam :double); stdcall;
type
  hProgressCallback = ^ProgressCallback;

type
  FinalCallback = procedure; stdcall;
type
  hFinalCallback = ^FinalCallback;

var
  Form1             :TForm1;
  progressThread    :TProgressThread;
  currentProgress   :double;

  realParamPath     :array of AnsiChar;
  realParamCopyPath :array of AnsiChar;

implementation

procedure ProcessFoldersAsync (
                                paths             :PAnsiChar;
                                copyPath          :PAnsiChar;
                                value             :double;
                                progressCallback  :hProgressCallback;
                                finalCallback     :hFinalCallback
                              );
stdcall;
external 'WAVEVolumeCorrector.dll';

{$R *.dfm}

procedure TProgressThread.UpdateProgress;
  begin;
    Form1.ProgressBar1.Position:=round(Form1.ProgressBar1.Max * currentProgress);
  end;

procedure TProgressThread.StopRender;
  begin;
    Form1.StringGrid1.RowCount:=0;
    Form1.StringGrid1.Rows[0].Clear;

    Form1.ActivityIndicator1.Animate:=false;
    Form1.ProgressBar1.Position:=0;

    Form1.Button2.Enabled       :=true;
    Form1.Button3.Enabled       :=true;
    Form1.Button4.Enabled       :=true;

    Form1.TrackBar1.Enabled     :=true;
    Form1.Edit1.Enabled         :=true;
    Form1.ToggleSwitch1.Enabled :=true;
  end;

procedure TProgressThread.Execute;
  begin;
    while not Terminated do
      Synchronize(UpdateProgress);

    Synchronize(StopRender);
  end;

procedure _ProgressCallback(inPercent: Double); stdcall;
  begin;
    currentProgress:=inPercent;
  end;

procedure _FinalCallback; stdcall;
  begin;
    if progressThread <> NIL then
      progressThread.Terminate;
  end;

procedure TForm1.Button1Click(Sender: TObject);
var
  buffer            :String;
  inputParameters   :String;
  copyPath          :String;

  index             :integer;
  begin;
    realParamCopyPath:=NIL;

    if ToggleSwitch1.State = tssOff then
      begin;
        if not SelectDirectory('Choose folder to save copies', '', copyPath) then
          Exit;

        if copyPath[Length(copyPath)] <> '\' then
          copyPath:=copyPath + '\';

        SetLength           (
                              realParamCopyPath,
                              Length(copyPath) + 1
                            );
        WideCharToMultiByte (
                              DefaultSystemCodePage,
                              0,
                              PWideChar(copyPath),
                              Length(copyPath) + 1,
                              PAnsiChar(realParamCopyPath),
                              Length(copyPath) + 1,
                              nil,
                              nil
                            );
      end;

    for index:=0 to StringGrid1.RowCount - 1 do
      begin;
        buffer:=StringGrid1.Cells[0, index];

        inputParameters:=inputParameters + buffer;

        SetLength           (
                              realParamPath,
                              Length(buffer) + 1
                            );
        WideCharToMultiByte (
                              DefaultSystemCodePage,
                              0,
                              PWideChar(buffer),
                              Length(buffer) + 1,
                              PAnsiChar(realParamPath),
                              Length(buffer) + 1,
                              nil,
                              nil
                            );

        if (GetFileAttributesA(PAnsiChar(realParamPath)) and FILE_ATTRIBUTE_DIRECTORY) <> 0 then
          begin;
            if inputParameters[Length(inputParameters)] <> '\' then
              inputParameters:=inputParameters + '\*'
            else
              inputParameters:=inputParameters + '*';
          end;

        inputParameters:=inputParameters + #0;
      end;

    inputParameters:=inputParameters + #0;

    SetLength           (
                          realParamPath,
                          Length(inputParameters) + 1
                        );
    WideCharToMultiByte (
                          DefaultSystemCodePage,
                          0,
                          PWideChar(inputParameters),
                          Length(inputParameters) + 1,
                          PAnsiChar(realParamPath),
                          Length(inputParameters) + 1,
                          nil,
                          nil
                        );

    Form1.ActivityIndicator1.Animate:=true;
    currentProgress:=0;
    progressThread:=TProgressThread.Create(false);

    ProcessFoldersAsync (
                          PAnsiChar(realParamPath),
                          PAnsiChar(realParamCopyPath),
                          strtofloat(Edit1.Text) / 100,
                          @_ProgressCallback,
                          hFinalCallback(@_FinalCallback)
                        );

    Button1.Enabled       :=false;
    Button2.Enabled       :=false;
    Button3.Enabled       :=false;
    Button4.Enabled       :=false;

    TrackBar1.Enabled     :=false;
    Edit1.Enabled         :=false;
    ToggleSwitch1.Enabled :=false;
  end;

procedure TForm1.Button2Click(Sender: TObject);
var
  path :string;
  begin;
    {if SelectDirectory(path, [], 0) then
      begin;
        if StringGrid1.Cells[0, 0] <> '' then
          StringGrid1.RowCount:=StringGrid1.RowCount + 1;

        StringGrid1.Cells[0, StringGrid1.RowCount - 1]:=path;
      end;}

    if SelectDirectory('Choose folder', '', path) then
      begin;
        if StringGrid1.Cells[0, 0] <> '' then
          StringGrid1.RowCount:=StringGrid1.RowCount + 1;

        StringGrid1.Cells[0, StringGrid1.RowCount - 1]:=path;

        if not Button1.Enabled then
          Button1.Enabled:=true;
      end;
  end;

procedure TForm1.Button3Click(Sender: TObject);
var
  index :integer;
  begin;
    if OpenDialog1.Execute then
      begin;
        for index:=0 to OpenDialog1.Files.Count - 1 do
          begin;
            if  StringGrid1.Cells[0, 0] <> '' then
              StringGrid1.RowCount:=StringGrid1.RowCount + 1;

            StringGrid1.Cells[0, StringGrid1.RowCount - 1]:=OpenDialog1.Files.Strings[index];
          end;

        if not Button1.Enabled then
          Button1.Enabled:=true;
      end;
  end;

procedure TForm1.Button4Click(Sender: TObject);
var
  index     :integer;
  starting  :integer;
  ending    :integer;
  length    :integer;
  begin;
    starting:=StringGrid1.Selection.Top;
    ending:=StringGrid1.Selection.Bottom;
    length:=ending - starting + 1;

    for index:=starting to StringGrid1.RowCount - length do
      StringGrid1.Rows[index].Assign(StringGrid1.Rows[index + length]);

    if StringGrid1.RowCount = length then
      begin;
        StringGrid1.Rows[0].Clear;
        Button1.Enabled:=false;
      end;

    StringGrid1.RowCount:=StringGrid1.RowCount - length;
  end;

procedure TForm1.FormCreate(Sender: TObject);
  begin;
    DragAcceptFiles(Handle, TRUE);

    StringGrid1.DefaultColWidth:=Screen.WorkAreaWidth;
  end;

procedure TForm1.FormDestroy(Sender: TObject);
  begin;
    DragAcceptFiles(Handle, FALSE);
  end;

procedure TForm1.DropFiles(var Msg: TWMDropFiles);
var
  filesCount      :Cardinal;
  fileNameLength  :Cardinal;
  buffer          :String;
  index           :Cardinal;
  begin;
    filesCount:=DragQueryFile(Msg.Drop, $FFFFFFFF, nil, 0) - 1;

    for index:=0 to filesCount do
      begin;
        FileNameLength:=DragQueryFile(Msg.Drop, index, nil, 0);
        SetLength(buffer, FileNameLength);

        DragQueryFile(Msg.Drop, index, PChar(buffer), FileNameLength + 1);

        if  StringGrid1.Cells[0, 0] <> '' then
          StringGrid1.RowCount:=StringGrid1.RowCount + 1;

        StringGrid1.Cells[0, StringGrid1.RowCount - 1]:=buffer;
      end;

    if (not Button1.Enabled           ) and
       (StringGrid1.Cells[0, 0] <> '' )
    then
      Button1.Enabled:=true;

    DragFinish(Msg.Drop);
  end;

procedure TForm1.Edit1Change(Sender: TObject);
var
  check :double;
  begin;
    try
      check:=strtofloat(Edit1.Text);

      if check > 200 then
        begin;
          Edit1.Text:='200';
          Exit;
        end;

      if check < 0 then
        Edit1.Text:='0';

    except
        Edit1.Text:=inttostr(TrackBar1.Position * 20);
      end;
  end;

procedure TForm1.Panel1MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
  begin;
    SendMessage(Handle, WM_SYSCOMMAND, WM_ENTERSIZEMOVE, 0);
  end;

procedure TForm1.Panel1MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
  begin;
    SendMessage(Handle, WM_SYSCOMMAND, WM_EXITSIZEMOVE, 0);
  end;

procedure TForm1.TrackBar1Change(Sender: TObject);
  begin;
    Edit1.Text:=inttostr(TrackBar1.Position * 20);
  end;

end.
